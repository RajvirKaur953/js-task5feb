function firstfun(){                                                     //function declaration
    document.write('Hello it is my first function in JS.', '<br/>');   //body of function
}
firstfun();                                                            //calling a function
var a=10;
var b=20;
function add(){
    return(a+b);                                                     //return is final statement after that no line will execute.
    document.write('Hello');
}
//parameterized function

function addition(x,y,z){
    return x+y+z;
}